#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include "atomic.h"

#define NITER (1<<14)
#define NRTHREADS 1000
#define NWTHREADS 50

struct seqlock l;
volatile long c;
volatile long d;
volatile long barrier;
volatile long checks;

void* reader(void *arg)
{
    int i;
    while (barrier < NWTHREADS) {
        long lc, ld;
        long s;
        do {
            s = seqlock_read_lock(&l);
            lc = c;
            ld = d;
        } while (!seqlock_read_unlock(&l, s));
        assert(lc == ld);
        atomic_add((long*) &checks, 1);
    }
    return 0;
}

void* writer(void *arg)
{
    int i;
    for (i = 0; i < NITER; i++) {
        seqlock_write_lock(&l);
        c++;
        d++;
        seqlock_write_unlock(&l);
    }
    atomic_add((long*) &barrier, 1);
    return 0;
}

void run_threads(void* (*r) (void*), void* (*w) (void*), void *arg)
{
    int i;
    pthread_t rtid[NRTHREADS];
    pthread_t wtid[NWTHREADS];
    for (i = 0; i < NRTHREADS; i++){
        pthread_create(&rtid[i], NULL, r, arg);
    }
    for (i = 0; i < NWTHREADS; i++){
        pthread_create(&wtid[i], NULL, w, arg);
    }
    for (i = 0; i < NRTHREADS; i++){
        pthread_join(rtid[i], NULL);
    }
    for (i = 0; i < NWTHREADS; i++){
        pthread_join(wtid[i], NULL);
    }
}

int main()
{
    seqlock_init(&l);
    run_threads(reader, writer, NULL);
    assert(checks > 0);
    printf("Passed test with %ld checks (c=%ld, d=%ld)\n", checks, c, d);

    return 0;
}
