#include "syscall.h"

int main()
{
    char buf[1000];
    int f = open("test_read.source", 0);
    ssize_t r = read(f, buf, 1000);
    if (r != 5 || buf[0] != 'w' || buf[1] != 'o' || buf[2] != 'r' || buf[3] != 'd' || buf[4] != '\n')
        return 1;
    return 0;
}
