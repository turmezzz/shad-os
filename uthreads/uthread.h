#pragma once

// Создаёт новый uthread и помещает его в конец очереди исполнения.
//
// Смысл аргументов f и data аналогичнен смыслу аргументов из
// pthread_create.
void uthread_start(void (*f)(void*), void* data);

// Останавливает текущий uthread, помещает его в конце очереди
// исполнения и запускает первый uthread из очереди исполнения.
void uthread_yield();

// Возвращает 1, если текущий uthread является единственным uthread в
// программе, возвращает 0 иначе.
int uthread_try_join();

